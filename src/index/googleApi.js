let googleApiKey = "AIzaSyBINxsZomMPw-LM58JUhynew55aBvOJo1E";
let googleClientId = "19821974199-fomtej0ptlj94ahn2mh2elpis9dglq9h.apps.googleusercontent.com";

function injectScript(src) {
  return new Promise((resolve, reject) => {
      const script = document.createElement('script');
      script.src = src;
      script.addEventListener('load', resolve);
      script.addEventListener('error', e => reject(e.error));
      document.head.appendChild(script);
  });
}

function authenticate() {
  return gapi.auth2.getAuthInstance()
      .signIn({scope: "https://www.googleapis.com/auth/youtube.readonly"})
      .then(function() { console.log("Api sign-in successful"); },
            function(err) { console.error("Error signing in", err); });
}
function loadClient() {
  gapi.client.setApiKey(googleApiKey);
  return gapi.client.load("https://www.googleapis.com/discovery/v1/apis/youtube/v3/rest")
      .then(function() { console.log("GAPI client loaded for API"); },
            function(err) { console.error("Error loading GAPI client for API", err); });
}

function getPromise(id){
  return gapi.client.youtube.videos.list({
    "part": [ "snippet" ],
    "id": [ id ]
  })
}

async function getResult(id){
  let r = await getPromise(id);
  r = r.result.items[0].snippet.title;
  return r;
}

async function getTitle(id) {
  let data = await getResult(id);
  return data;
}

function setupApi(){
  injectScript('https://apis.google.com/js/api.js').then(() => {
    console.log('Google Api loaded');
    gapi.load("client:auth2", function() {
      gapi.auth2.init({client_id: googleClientId});
      authenticate().then(loadClient);
    });
  }).catch(error => {
    console.error(error);
  });
}

export { setupApi, getTitle };