import '../style/style.scss';
import { setupApi, getTitle } from './googleApi';
import { v4 as uuidv4 } from 'uuid';

let jsonBinApiKey = "$2b$10$AJofEf9tkowRMie3ap6jTOU4H0cOukbsiD9xbYSBoW/sX7tVb5rdq"
let binID = "6344933d65b57a31e691f293";

const manager = new Object;
window.manager = manager;

const root = document.getElementById('root');

document.onreadystatechange = () => {
  if (document.readyState === 'complete') {
    setupApi();
    readData(buildPage);
    console.log('Page is ready');
  }
}

document.querySelector('#new-song form').onsubmit = (event) => {
  event.preventDefault();
  let songUrl = event.target.elements['song-url'].value;
  let songId = uuidv4();
  let time = new Date();
  let songTimestamp = `${time.getFullYear()}${time.getMonth()}${time.getDay()}${time.getHours()}${time.getMinutes()}${time.getSeconds()}`;
  let newSong = {
    url: songUrl,
    tags: [],
    timestamp: songTimestamp,
    id: songId
  };
  manager.data.songs.push(newSong);
  updateData();
  buildPage(songId);
  document.querySelector('#new-song form').reset();
}

function readData(callback = ()=>{}) {
  let req = new XMLHttpRequest();

  req.onreadystatechange = () => {
    if (req.readyState == XMLHttpRequest.DONE) {
      manager.data = JSON.parse(req.responseText);
      console.log("Data retrieved");
      callback();
    }
  };

  req.open("GET", `https://api.jsonbin.io/v3/b/${binID}/latest`, true);
  req.setRequestHeader("X-Master-Key", jsonBinApiKey);
  req.setRequestHeader("X-Bin-Meta", false);
  req.send();
}

function updateData() {
  let req = new XMLHttpRequest();

  req.onreadystatechange = () => {
    if (req.readyState == XMLHttpRequest.DONE) {
      //console.log(req.responseText);
      console.log("JSON Bin updated");
    }
  };

  req.open("PUT", `https://api.jsonbin.io/v3/b/${binID}`, true);
  req.setRequestHeader("Content-Type", "application/json");
  req.setRequestHeader("X-Master-Key", jsonBinApiKey);
  req.send(JSON.stringify(manager.data));
}

function buildPage(song = null) {
  if (document.getElementById('content')) { root.removeChild(document.getElementById('content')); }

  let pageContent = document.createElement('div');
  pageContent.setAttribute('id', 'content');
  root.append(pageContent);
  let currentSong = document.createElement('div');
  currentSong.setAttribute('id', 'current-song');
  let navigator = document.createElement('div');
  navigator.setAttribute('id', 'navigator');
  navigator.append(currentSong);
  let songNavigatorWrapper = document.createElement('div');
  songNavigatorWrapper.setAttribute('id', 'navigator-songs');
  songNavigatorWrapper.classList.add('navigator');
  let songTitle = document.createElement('h2');
  songTitle.innerHTML = "Canzoni";
  songNavigatorWrapper.append(songTitle);
  manager.data.songs.forEach((e) => {
    let element = document.createElement('div');    
    let id = e.url.split('https://www.youtube.com/watch?v=')[1];
    getTitle(id).then((response) => { element.innerHTML = response;});
    element.setAttribute('video-id', id);
    element.classList.add('song');    
    element.onclick = () => { updateCurrentSong(e.id); };
    songNavigatorWrapper.append(element);
  });
  updateTagNavigator(navigator);
  navigator.append(songNavigatorWrapper);
  pageContent.append(navigator);
  if (song) { updateCurrentSong(song) };
  let explorer = document.createElement('div');
  explorer.setAttribute('id', 'explorer');
  let tagExplorer = document.createElement('div');
  explorer.append(tagExplorer);
  let explorerTitle = document.createElement('h2');
  explorerTitle.innerHTML = 'Tag Explorer';
  explorer.append(explorerTitle);
  pageContent.append(explorer);
}

function updateTagNavigator(navigator) {
  let tagNavigatorWrapper = document.createElement('div');
  tagNavigatorWrapper.setAttribute('id', 'navigator-tags');
  tagNavigatorWrapper.classList.add('navigator');
  let tagTitle = document.createElement('h2');
  tagTitle.innerHTML = "Tag";
  tagNavigatorWrapper.append(tagTitle);
  let tagList = document.createElement('div');
  manager.data.tags.forEach((e) => {
    let element = document.createElement('div');    
    element.innerHTML = e.name;    
    element.classList.add('tag');
    element.onclick = () => { updateTagExplorer(e.id); };
    tagList.append(element);
  });
  let navigatorTags = navigator.querySelector('#navigator-tags');
  if (navigatorTags) { navigator.removeChild(navigatorTags); }
  tagNavigatorWrapper.append(tagList)
  navigator.append(tagNavigatorWrapper);
}

function updateCurrentSong(id) {
  const current = document.getElementById('current-song');
  while (current.lastChild) { current.removeChild(current.lastChild); }
  let song = manager.data.songs.find(value => value.id === id);
  let songTitle = document.createElement('h3');
  let uri = song.url.split('https://www.youtube.com/watch?v=')[1];
  getTitle(uri).then((response) => { songTitle.innerHTML = response;});
  current.append(songTitle);
  let songEmbed = document.createElement('iframe');
  songEmbed.setAttribute('width', '560');
  songEmbed.setAttribute('height', '315');
  let v = song.url.split('https://www.youtube.com/watch?v=')[1];
  songEmbed.setAttribute('src', `https://www.youtube.com/embed/${v}?controls=0`);
  songEmbed.setAttribute('title', 'YouTube video player');
  songEmbed.setAttribute('frameborder', '0');
  songEmbed.setAttribute('allow', 'accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture');
  current.append(songEmbed);
  let tagContainer = document.createElement('div')
  manager.data.tags
    .filter((value) => { return song.tags.some(tag => tag === value.id); })
    .forEach((value,i, n, a) => {
      let tagElement = document.createElement('span');
      tagElement.classList.add('tag');
      tagElement.innerHTML = value.name;
      tagElement.onclick = () => { updateTagExplorer(value.id); };
      tagContainer.append(tagElement);
    }
  );
  let tagForm = document.createElement('form');
  let tagLabel = document.createElement('label');  
  let tagInput = document.createElement('input');
  tagInput.setAttribute('name', 'newTag');
  let tagButton = document.createElement('button');
  tagButton.textContent = "Add tag";
  tagLabel.append(tagInput);
  tagForm.append(tagLabel);
  tagForm.append(tagButton);
  tagForm.onsubmit = (event) => {
    event.preventDefault();
    let tagName = event.target.elements['newTag'].value.toLowerCase();
    let tagExists = manager.data.tags.find(v => v.name === tagName);
    let newTag;
    if (tagExists) {
      newTag = tagExists;
    } else {
      let tagId = uuidv4();
      newTag = {
        name: tagName,
        id: tagId
      };
      manager.data.tags.push(newTag);
    }
    let index = manager.data.songs.findIndex(value => value.id === id);
    manager.data.songs[index].tags.push(newTag.id);
    updateData();
    updateCurrentSong(id);
    updateTagNavigator(document.getElementById('navigator'));
  };
  tagContainer.append(tagForm);
  current.append(tagContainer);
}

function updateTagExplorer(id) {
  let explorer = document.getElementById('explorer');
  while (explorer.lastChild) { explorer.removeChild(explorer.lastChild); }
  let explorerTitle = document.createElement('h2');
  explorerTitle.innerHTML = 'Tag Explorer<br><i>' + manager.data.tags.find(value => value.id === id).name.toUpperCase() + '</i>';  
  explorer.append(explorerTitle);
  manager.data.songs
    .filter(value => value.tags.some(tag => tag === id))
    .forEach(value => {
      let element = document.createElement('p');
      let uri = value.url.split('https://www.youtube.com/watch?v=')[1];
      getTitle(uri).then((response) => { element.innerHTML = response;});
      element.setAttribute('video-id', id);
      element.classList.add('song');
      element.onclick = () => {
        updateCurrentSong(value.id);
      }
      explorer.append(element);
    });
}
