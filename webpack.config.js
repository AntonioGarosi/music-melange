const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');

const pages = ['index'];

const modules = {
	rules: [
		{
			test: /\.css$/,
			use: [
				'style-loader',
				'css-loader',
				'postcss-loader'
			]
		},
		{
			test: /\.s[ac]ss$/i,
			use: [
				'style-loader',
				'css-loader',				
				'postcss-loader',
				'sass-loader'
			]
		},
		{
			test: /\.(png|svg|jpg|jpeg|gif)$/,
			use: [
				'file-loader'
			]
		},
		{
			test: /\.html$/i,
			loader: 'html-loader'
		}
	]
};

const devServerConfig = {
	compress: true,
	watchFiles: ['./src/*']		
};

module.exports = {
	mode: 'development',
	entry: 
		pages.reduce((config, page) => {
			config[page] = `./src/${page}/main.js`;
			return config;
		}, {}),
	output: {
		filename: '[name].js',
		path: path.resolve(__dirname, 'dist'),
	},
	optimization: {
    splitChunks: {
      chunks: "all",
    },
  },
	module: modules,
	devServer: devServerConfig, 
	plugins: [].concat(
		pages.map((page) => 
			new HtmlWebpackPlugin({
				inject: true,
				template: path.resolve(__dirname, `./src/${page}/index.html`),
				filename: `${page}.html`,
				chunks: [page],
			})
		)
	)
};